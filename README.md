# Megaman Legends Project Dashboard

This repository is an archive of tools for reverse egineering the asset formats from the Megaman Legends series on Playstations. 
The result of these tools have been combined into save state viewers which are the definitive versions for viewing and exporting models
ans will be listed at the top. Following that will be a list of legacy tools which have been included in this repository.

**Definitive Versions**

- Megaman Legends 1
- Misadventures of Tron Bonne
- Megaman Legends 2

**Megaman Legends 1 Tools**

- Ebd Viewer
- Stg Viewer
- Texture Editor
- Low Poly Rom Hack
- Islands Engine 

**Archived Tools**

- Megaman Legends 1 PBD viewer
- Megaman Lgends 2 PC Models
- Megaman Legends 2 PSX Mesh
- Megaman Legends 1 Anim Edition
- Megaman Legends 1 Debug Edition
- Texture Viewer
- DashViewer (xDaniel)