/*
* Copyright 2017,2018 Benjamin Collins
*
* Dash Texture View is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any 
* later version.
*
* Dash-Model-Viewer is distributed in the hope that it will be useful, but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
* details.
*
* You should have received a copy of the GNU General Public License along with 
* Dash Texture View. If not, see http://www.gnu.org/licenses/.
*/


"use strict";

const file = document.getElementById("file");
file.addEventListener("change", function(evt) {
	
	var bin = evt.target.files[0];
	if(!bin) {
		return;
	}
	
	window.filename = bin.name;
	var reader = new FileReader();

	reader.onload = function(e) {
		
		var array_buffer = e.target.result;
		window.buffer = new DataView(array_buffer);
		read_headers();

	}

	reader.readAsArrayBuffer(bin);

});


function read_headers() {

	console.log("read headers!!");

	let base = window.filename.split(".");
	base.pop();
	base = base.pop();

	let ofs = 0;
	const fp = window.buffer;
	fp.length = window.buffer.byteLength;
	var select = document.getElementById("select");
	select.innerHTML = "";
	window.active = null;

	let tcount = 0;

	do {
		
		const num = fp.getUint32(ofs + 0x00, true);
		const length = fp.getUint32(ofs + 0x04, true);

		if(num !== 3) {
			ofs += (length & 0x400);
			continue;
		}

		console.log("TIM FILE FOUND AT: 0x%s", ofs.toString(16));
	
		const tim = {
			offset : ofs,
			decompressed_size : fp.getUint16(ofs + 0x04, true),

			pallet_x : fp.getUint16(ofs + 0x0c, true),
			pallet_y : fp.getUint16(ofs + 0x0e, true),

			nb_colors : fp.getUint16(ofs + 0x10, true),
			nb_pallet : fp.getUint16(ofs + 0x12, true),

			image_x : fp.getUint16(ofs + 0x14, true),
			image_y : fp.getUint16(ofs + 0x16, true),

			width : fp.getUint16(ofs + 0x18, true),
			height : fp.getUint16(ofs + 0x1a, true),

			bitfield_size : fp.getUint16(ofs + 0x24, true),
			name : Math.random().toString(36).substring(7)
		};

		console.log(tim);

		if(tim.width === 0 || tim.height === 0) {
			continue;
		}
		
		switch(tim.nb_colors) {
			case 16:
				tim.width *= 4;
			break;
			case 256:
				tim.width *= 2;
			break;
			default:
				return console.log("Color count: %d", tim.nb_colors);
				break;
		}

		tim.pallet = new Array(tim.nb_pallet);
		var tmp_ofs = ofs + 0x30;

		let str = tcount.toString();
		while(str.length < 3) {
			str = "0" + str;
		}

		var li = document.createElement("li");
		li.textContent = base + "_" + num;
		li.addEventListener("click", preview_texture.bind(null, li, tim));
		select.appendChild(li);

		if(tcount === 0) {
			preview_texture(li, tim);
		}

		tcount++;

	} while((ofs += 0x400) < fp.length);

}

function preview_texture(li, tim) {

	if(window.active) {
		window.active.removeAttribute("class");
	}
	
	li.setAttribute("class", "selected");
	window.active = li;

	const fp = window.buffer;

	// First we need to decompress the data

	const bitField = [];

	let ofs = tim.offset + 0x30;
	for(let i = 0; i < tim.bitfield_size; i += 4) {
		
		let byte = fp.getUint32(ofs, true);
		ofs += 4;

		for(let k = 31; k > -1; k--) {
			let bit = 1 << k;
			bitField.push((byte & bit) ? 1 : 0);
		}
	}
	
	tim.buffer = new ArrayBuffer(tim.decompressed_size);
	tim.fp = new DataView(tim.buffer);

	// Then we decompress the bytes

	let out_ofs = 0;
	let	window_ofs = 0;

	for(let i = 0; i < bitField.length; i++) {

		let bit = bitField[i];
		let word = fp.getUint16(ofs, true);

		if(bit === 0) {

			tim.fp.setUint16(out_ofs, word, true);
			out_ofs += 2;

		} else if(word === 0xffff) {

			window_ofs += 0x2000;

		} else {
			
			let val = ((word >> 3) & 0x1fff);
			let copyFrom = window_ofs + val;
			let copyLen = (word & 0x07) + 2;

			while(copyLen--) {
				let w = tim.fp.getUint16(copyFrom, true);
				copyFrom+=2;
				tim.fp.setUint16(out_ofs, w, true);
				out_ofs+=2;
			}

		}
		
		if(out_ofs >= tim.decompressed_size) {
			break;
		}

		ofs += 2;

	}

	console.log("File position: 0x%s", ofs.toString(16));
	console.log("Final positon: 0x%s", out_ofs.toString(16));

	tim.tmp_ofs = 0;
	for(var i = 0; i < tim.nb_pallet; i++) {
		tim.pallet[i] = new Array(tim.nb_colors);

		for(var k = 0; k < tim.nb_colors; k++) {
			tim.pallet[i][k] = tim.fp.getUint16(tim.tmp_ofs, true);
			tim.tmp_ofs += 0x02;
			var r = ((tim.pallet[i][k] >> 0x00) & 0x1f) << 3;
			var g = ((tim.pallet[i][k] >> 0x05) & 0x1f) << 3;
			var b = ((tim.pallet[i][k] >> 0x0a) & 0x1f) << 3;
			var a = tim.pallet[i][k] > 0 ? 1 : 0;
		}

	}

	display_texture(0, tim);

}

function change_pallet(select, tim) {

	var index = parseInt(select.selectedIndex);
	display_texture(index, tim);

}

function display_texture(index, tim) {

	var pallet = tim.pallet[index];

	var inc = 1;
	var block_width = 64;
	var block_height = 32;

	if(tim.nb_colors === 16) {
		inc *= 2;
		block_width *= 2;
	}
	
	var ofs = tim.tmp_ofs;
	var image_body = new Array(tim.width * tim.height);
	var y, x, by, bx, pos, byte;

	let image_length = tim.decompressed_size - tim.tmp_ofs;

	var image_body = new Array();
	for(var k = 0; k < image_length; k++) {
		var byte = tim.fp.getUint8(ofs + k);

		if(image_length === 0x10000) {
			image_body.push(pallet[byte]);
		} else if(tim.nb_colors === 0x100) {
			image_body.push(pallet[byte]);
		} else {
			image_body.push(pallet[(byte & 0xf)]);
			image_body.push(pallet[(byte >> 4)]);
		}
	}
			
	let image_width = 256;
	let image_height = 256;
			
	var canvas = document.getElementById("canvas");
	canvas.width = 256;
	canvas.height = 256;
	var ctx = canvas.getContext("2d");

	if(image_length === 0x2000) {
		image_width = 128;
	}

	var ofs = 0;
	for(var y = 0; y < image_height; y++) {
		for(var x = 0; x < image_width; x++) {
					
			var r = ((image_body[ofs] >> 0x00) & 0x1f) << 3;
			var g = ((image_body[ofs] >> 0x05) & 0x1f) << 3;
			var b = ((image_body[ofs] >> 0x0a) & 0x1f) << 3;
			var a = image_body[ofs] > 0 ? 1 : 0;
			ctx.fillStyle = "rgba("+r+","+g+","+b+","+a+")";
			ctx.fillRect(x, y, 1, 1);
			ofs++;
		}
	}

}


const exp = document.getElementById("export");
exp.addEventListener("click", function(evt) {
	if(!window.active) {
		return;
	}
	var canvas = document.getElementById("canvas");
	var name = window.active.textContent;
	var str = name.substr(0, name.indexOf(".")) + ".PNG";

	canvas.toBlob(function(blob) {
		saveAs(blob, str);
	}, "image/png");

});

